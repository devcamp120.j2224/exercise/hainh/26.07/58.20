package com.example.demo.Respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.Model.DrinkList;

public interface iDrinkRespository extends JpaRepository<DrinkList ,Long>{
    
}
