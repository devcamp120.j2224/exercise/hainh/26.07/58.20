package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.DrinkList;
import com.example.demo.Respository.iDrinkRespository;

@RestController
@CrossOrigin
public class DrinkListController {
    
    @Autowired
    iDrinkRespository iDrinkRespository ;

    @GetMapping("drinks")
    public ResponseEntity <List <DrinkList>> getDrinks() {
        
        try {
            List<DrinkList> listDrink = new ArrayList<DrinkList>();
            // object. findAll là hàm tìm kiếm tất cả . forEach và add hết tất cả cho arraylist 
            iDrinkRespository.findAll().forEach(listDrink :: add);

            if(listDrink.size() == 0){
                return new ResponseEntity<List <DrinkList>>(listDrink, HttpStatus.NOT_FOUND);
            }
            else{
                return new ResponseEntity<List <DrinkList>>(listDrink, HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
